package datatypes;

public class DataTypes1 {

    public static void main(String[] args) {
        // syso + control + space bar --> println shortcut
        // *****-----Data Types -----*****
        String name = "Rion";
        String name2 = "Azhar";
        int salary = 120;
        int salary2 = 100;
        boolean car = false;
        // String -- data types
        // name -- bucket/variable
        // Rion -- value

        System.out.println("My Name is " + name);
        System.out.println(name + "'s salary is 120k");
        System.out.println(name + "'s bonus is 10%");

        // put your name in a string variable and print
        //"Rion's dob is 14th feb" and "Rion's fav food is kacchi biriyani"

        System.out.println(name + "'s salary is " + salary);
        System.out.println(salary);
        System.out.println(name);

        // boolean -- true/ false
        System.out.println("I have a car. is that " + car);

        System.out.println(car + " " + name + " " + salary);

        System.out.println(name + name2);
        System.out.println(salary + salary2);
        System.out.println(salary + " " + salary2);
        System.out.println(salary - salary2);
        System.out.println(salary * salary2);
        System.out.println(salary / salary2);

    }

}
