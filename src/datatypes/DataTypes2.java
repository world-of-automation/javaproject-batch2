package datatypes;

public class DataTypes2 {

    public static void main(String[] args) {

        //\byte - 8 bit
        byte number1 = -128;
        byte number2 = 127;

        // short - 16 bit
        short number3 = -32768;
        short number4 = 32767;

        // int - 32 bit
        int number5 = -2_147_483_648;
        int number6 = 2_147_483_647;

        // long - 64 bit
        long number7 = 2453457234242445345l;

        //double - decimal - 64bit (d is optional)
        double number8 = 1.23d;

        // float - 32 bit - f
        float number9 = 123.23f;

        //char - character- unicode - 16bit
        //https://en.wikipedia.org/wiki/List_of_Unicode_characters
        char character = '1';
        char copyright = '\u00A9';

        System.out.println(copyright);

        //boolean - true/false
        boolean box = true;

    }
}
