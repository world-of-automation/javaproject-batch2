package datastructure;

public class ArrayPractice {

    public static void main(String[] args) {

        //String name = "Azhar";
        String[] names = {"Azhar", "Rion", "Zann"};
        System.out.println(names.length);
        System.out.println(names[1]);

        for (int i = 0; i < names.length; i++) {
            System.out.print(names[i] + " ");
        }
        System.out.println();

        Object[] everything = {1, "Rion", true,};
        System.out.println(everything[1]);

        // dynamic array
        String[] cars = new String[4];
        cars[0] = "Honda";
        cars[1] = "Toyota";
        cars[2] = "Acura";


        System.out.println(cars.length);
        System.out.println(cars[1]);

        // System.out.println(cars[4]); ----> throws ArrayIndexOutOfBoundsException

    }
}
