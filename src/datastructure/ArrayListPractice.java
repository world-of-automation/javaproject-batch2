package datastructure;

import java.util.ArrayList;

public class ArrayListPractice {

    public static void main(String[] args) {

        ArrayList<String> arrayListOfState = new ArrayList<>();

        arrayListOfState.add("NY");
        arrayListOfState.add("NJ");
        arrayListOfState.add("VA");
        arrayListOfState.add("DC");
        arrayListOfState.add(null);
        arrayListOfState.add(null);

        arrayListOfState.remove("DC");
        System.out.println(arrayListOfState.size());

        System.out.println(arrayListOfState);

        System.out.println(arrayListOfState.get(2));

        arrayListOfState.add("CA");
        System.out.println(arrayListOfState.size());
        System.out.println(arrayListOfState);

        String data;
        for (int i = 0; i < arrayListOfState.size(); i++) {
            data = arrayListOfState.get(i);
            if (data.equalsIgnoreCase("VA")) {
                System.out.println("virginia is the state for the lovers");
                break;
            }
        }

        // all types of data
        ArrayList datas = new ArrayList();
        datas.add(1);
        datas.add("Azhar");
        datas.add(12f);

        ArrayList<Object> dataList = new ArrayList<>();
        dataList.add("rion");
        dataList.add(12);
        dataList.add(true);


        // numbers
        ArrayList<Integer> zipCodes = new ArrayList<>();
        zipCodes.add(11374);


        ArrayList<ArrayList<String>> usa = new ArrayList<>();

        ArrayList<String> cities = new ArrayList<>();
        cities.add("Rego Park");
        cities.add("Ozone Park");
        cities.add("Jamaica");

        ArrayList<String> zipCode = new ArrayList<>();
        zipCode.add("11374");
        zipCode.add("23423");
        zipCode.add("4232");
        zipCode.add("23424");
        zipCode.add("23456");

        usa.add(cities);
        usa.add(zipCode);
        System.out.println(usa);
        System.out.println(usa.size());

        System.out.println(usa.get(0));
        System.out.println(usa.get(1));

        System.out.println(usa.get(0).get(0));
        System.out.println(usa.get(1).get(1));


        for (int i = 0; i < usa.size(); i++) {
            for (int j = 0; j < usa.get(i).size(); j++) {
                System.out.println(usa.get(i).get(j));
            }
        }

        for (int i = 0; i < usa.size(); i++) {
            ArrayList arrayList = usa.get(i);
            for (int j = 0; j < arrayList.size(); j++) {
                System.out.println(arrayList.get(j));
            }
        }


    }
}
