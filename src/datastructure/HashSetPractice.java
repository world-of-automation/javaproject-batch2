package datastructure;

import java.util.HashSet;

public class HashSetPractice {

    public static void main(String[] args) {
        HashSet<String> hashSet = new HashSet<>();
        hashSet.add("Azhar");
        hashSet.add("Rion");
        hashSet.add("Azhar");
        hashSet.add(null);
        System.out.println(hashSet);
        System.out.println(hashSet.size());
    }
}
