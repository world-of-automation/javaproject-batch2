package datastructure;

import java.util.ArrayList;
import java.util.HashMap;

public class HashMapPractice {

    public static void main(String[] args) {

        HashMap<String, String> country = new HashMap<>();
        country.put("name", "USA");
        country.put("president", "Obama");
        country.put("indipendence", "1816");
        country.put(null, "something");
        country.put("president", "Trump");
        System.out.println(country.get(null));
        System.out.println(country.get("president"));
        System.out.println(country);

        ArrayList<String> keysAsArrayList = new ArrayList<>(country.keySet());

        for (int i = 0; i < country.size(); i++) {
            System.out.println(country.get(keysAsArrayList.get(i)));
        }

        HashMap<String, HashMap<String, String>> mapOfWorld = new HashMap<>();

        HashMap<String, String> statesOfUSA = new HashMap<>();
        statesOfUSA.put("NY", "Manhattan");
        statesOfUSA.put("VA", "SOmething");

        HashMap<String, String> statesOfCanada = new HashMap<>();
        statesOfCanada.put("ML", "Restaurent");
        statesOfCanada.put("SC", "Everything");

        mapOfWorld.put("canada", statesOfCanada);
        mapOfWorld.put("usa", statesOfUSA);

        System.out.println(mapOfWorld);

        System.out.println(mapOfWorld.get("usa"));

        System.out.println(mapOfWorld.get("usa").get("VA"));


        HashMap<String, ArrayList<String>> arrayListMap = new HashMap<>();

        ArrayList<String> countries = new ArrayList<>();
        countries.add("USA");
        countries.add("Canada");

        ArrayList<String> states = new ArrayList<>();
        states.add("NY");
        states.add("NJ");

        arrayListMap.put("name of the countries", countries);
        arrayListMap.put("name of the states", states);

        System.out.println(arrayListMap);

        System.out.println(arrayListMap.get("name of the countries").get(0));
    }

}
