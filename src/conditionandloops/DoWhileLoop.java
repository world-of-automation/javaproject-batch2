package conditionandloops;

public class DoWhileLoop {

    public static void main(String[] args) {

        int x = 0;
        do {
            System.out.println("something");
            x++;
        }
        while (x < 5);

    }
}
