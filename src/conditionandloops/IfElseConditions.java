package conditionandloops;

public class IfElseConditions {

    public static void main(String[] args) {
        int number = 30;

        //if --keyword (condition){-- if body starts
        // if body inside
        // } --if body ended
        // else -- keyword { -- else body starts
        // else body inside
        // }-- else body ended

        if (number == 30) {
            System.out.println("today is eid day");
        } else {
            System.out.println("today is not eid day");
        }

        String name = "Rion";
        if (name.equalsIgnoreCase("Rion")) {
            System.out.println("his name is matching");
        } else {
            System.out.println("his name didn't match");
        }


        if (name.equalsIgnoreCase("Rion")) {
            System.out.println("his car is toyota");
        } else if (name.equalsIgnoreCase("Azhar")) {
            System.out.println("his car is Pilot");
        } else {
            System.out.println("i don't anymore");
        }


    }
}
