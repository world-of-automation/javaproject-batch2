package conditionandloops;

public class IfElseConditions2 {

    public static void main(String[] args) {

        int number1 = 10;
        int number2 = 20;
        int number3 = 5;

        if (number1 < number2) {
            System.out.println("number1 is less than number2");
        } else if (number1 > number2) {
            System.out.println("number1 is greater than number2");
        } else if (number1 == number2) {
            System.out.println("number1 is equal to number2");
        }

        if (number1 != number2) {
            System.out.println("number 1 is not equal to number2");
        }

        // || - or
        // && - and
        // != - not equal
        // == - equal

        if (number1 < number2 && number1 < number3) { // -- and condition
            System.out.println(" number1 is less than number2 and 5");
        }


        if (number1 < number2 || number1 < number3) { // -- or condition
            System.out.println(" number1 is less than number2 or 5");
        }

    }
}
