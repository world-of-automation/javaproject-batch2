package conditionandloops;

public class InnerForLoop {

    public static void main(String[] args) {

        // print selenium 10 times ,
        // and for every time of selenium, print java 2 times


        for (int i = 0; i < 10; i++) {
            System.out.println("selenium");
            for (int j = 0; j < 2; j++) {
                System.out.println("java");
            }
        }

    }
}
