package conditionandloops;

public class ForLoops {

    public static void main(String[] args) {

        //for --keyword (starting point ; end point ; increment/decrement ) { --for body starts
        // for body
        // } -- for body ends

        for (int i = 0; i < 10; i++) {
            System.out.println("My Name Is Rion" + i);
        }

        for (int i = 0; i > 10; i--) {
            System.out.println("My Name Is Rion");
        }

    }
}
