package conditionandloops;

public class WhileLoop {

    public static void main(String[] args) {
        int x = 0;
        while (x < 5) {
            System.out.println("My Name Is Rion" + x);
            x++;
        }

        System.out.println("*********");

        boolean flag = false;
        while (flag != true) {
            boolean found = false;
            System.out.println("trying to find the data from database");
            if ("x" == "x") { // checking if data is found
                found = true;
            }
            flag = found;
        }
    }
}
