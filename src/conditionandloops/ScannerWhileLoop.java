package conditionandloops;

import java.util.Scanner;

public class ScannerWhileLoop {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        boolean flag = false;
        while (flag != true) {
            System.out.println("if your name is rion enter true ");
            boolean userInput = scanner.nextBoolean();

            if (userInput == true) {
                flag = true;
            }
        }
    }
}
