package refresh.oop;

public interface ConceptOfCar {

    // no constructor
    // can't create object
    // we need implement interface into a regular class to implement the methods
    // can have return type method
    // can't have static methods


    // concept --> no implementation --> no method body
    // only method name --> no method body


    int wheels();

    void start();

    void stop();


}
