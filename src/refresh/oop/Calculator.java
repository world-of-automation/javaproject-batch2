package refresh.oop;

public class Calculator {

    // same method name , diff params, same class --> method overloading
    // static polymorphism/ compile time polymorphism

    public int addition(int a, int b, int c) {
        int d = a + b + c;
        return d;
    }

    public int addition(int a, int b) {
        int d = a + b;
        return d;
    }

    // same method name , same params, different class --> method overriding
    // dynamic polymorphism/ runtime polymorphism
    // example at Ford class

}
