package refresh.oop;

public class UserBoughtFord {

    public static void main(String[] args) {
        Ford ford = new Ford();
        ford.start();
        ford.stop();
        ford.color();
        int wheels = ford.wheels();
        System.out.println(wheels);

        ford.brakeForCar();
    }
}
