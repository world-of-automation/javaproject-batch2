package refresh.oop;

public class Ford extends SolidIdeasForCar implements ConceptOfCar {

    // you extend first , then implement
    // others(abstract class)--> extends
    // i - i --> interface--> implements

    // method overriding
    @Override
    public int wheels() {
        return 4;
    }

    @Override
    public void start() {
        System.out.println("ford found a way to start the engine");
    }

    @Override
    public void stop() {
        System.out.println("ford found a way to stop the engine");
    }

    @Override
    public void color() {
        System.out.println("ford is red");
    }
}
