package refresh.oop;

public class C extends B {

    //child of B
    // java doesn't support multiple inheritance for a class
    // a class can have multiple children but not multiple parents

    public void printDayFromC() {
        System.out.println("Monday");
    }

    // inheritance -- parent - child class(A,B,C)
    // abstraction -- interface/abstract class
    // polymorphism -- calculator
    // encapsulation -- setter, getter


}
