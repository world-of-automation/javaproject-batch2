package refresh.oop;

public class ABC {
    public static void main(String[] args) {

        A a = new A();
        a.printYearFromA();

        B b = new B();
        b.printMonthsFromB();
        b.printYearFromA();


        C c = new C();
        c.printDayFromC();
        c.printMonthsFromB();
        c.printYearFromA();
    }
}
