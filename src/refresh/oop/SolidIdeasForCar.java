package refresh.oop;

public abstract class SolidIdeasForCar {


    // can have a constructor --> but we can not create a obj of abstract class
    // can have a method with body or method without body
    // method without body --> we have to specify with the keyword abstract before we specify void or return type


    // either implementation or not

    // abstract class--> empty method--> have to specify abstract keyword in the method name
    // but for interface we dont

    public void brakeForCar() {
        System.out.println(" we need to have a brake for the car");
    }

    public abstract void color();
}
