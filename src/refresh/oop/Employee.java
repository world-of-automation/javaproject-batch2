package refresh.oop;

public class Employee {

    private int id;
    private String name;

    //getter--> return type of the associated data
    public int getId() {
        return id;
    }

    //setter --> void
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
