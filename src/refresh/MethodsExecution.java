package refresh;

public class MethodsExecution {

    public static void main(String[] args) {


        // static--> class name . method name

        // void
        MethodsPractice.printMyName();
        MethodsPractice.calculateMySalary(40, 75);
        //return
        String coffeeFromAzharVai = MethodsPractice.getMeACoffee();
        System.out.println(coffeeFromAzharVai);

        //non static--> create obj
        // void
        MethodsPractice methodsPractice = new MethodsPractice();
        //class obj = new constructorName();

        methodsPractice.printMySchool();

        int year = methodsPractice.getYear();
        System.out.println(year);

        double netSalary = methodsPractice.calculateAndGetMySalary(20, 45, .20);
        System.out.println("net salary is : " + netSalary);


    }
}
