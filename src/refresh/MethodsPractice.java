package refresh;

public class MethodsPractice {

    static String nameOfMonth = "April";//--> static

    // static
    //***********class level variables***********
    //default --> not specified anything
    // private, public, protected
    // public -- can be accessed anywhere in the project
    // private -- can be accessed in the same class only
    String company = "world-of-automation";//-->non static

    //default constructor
    public MethodsPractice() {

    }


    // non static

    // void
    public static void printMyName() {
        System.out.println("Zann");
    }

    //return--> any data types
    public static String getMeACoffee() {
        return "coffee from dunkin";
    }


    //***********PARAMETERIZED METHODS***********

    public static void calculateMySalary(int hours, int payrate) {
        double grossSalary = hours * payrate;
        double netSalary = grossSalary - (grossSalary * .23);
        System.out.println("grossSalary is : " + grossSalary);
        System.out.println("netSalary is : " + netSalary);
    }

    public static void testVariables2() {
        //System.out.println(company);
        System.out.println(nameOfMonth);
    }

    public static void testMethodsStatic() {
        // object
        MethodsPractice methodsPractice = new MethodsPractice();
        methodsPractice.testVariables1();
        testVariables2();
    }

    // void
    public void printMySchool() {
        System.out.println("MUB");
    }

    //return
    public int getYear() {
        int previousYear = 2019;
        int currentYear = previousYear + 1;
        return currentYear;
    }

    public double calculateAndGetMySalary(int hours, int payrate, double taxPercent) {
        double grossSalary = hours * payrate;
        double netSalary = grossSalary - (grossSalary * taxPercent);
        return netSalary;
    }

    //static can go either on static or non static methods
    // static --> everywhere
    //non static can go only on non static methods
    // nonstatic --> non static
    public void testVariables1() {
        System.out.println(company);
        System.out.println(nameOfMonth);
    }

    // non static method to call inside static method we need object

    public void testMethodsNonStatic() {
        testVariables1();
        testVariables2();
    }


}
