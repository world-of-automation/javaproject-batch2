package refresh.datastructure;

import java.util.HashMap;

public class HashMapPractice {

    public static void main(String[] args) {


        HashMap<String, String> mapOfNewYork = new HashMap<>();

        mapOfNewYork.put("Rego Park", "11374");
        mapOfNewYork.put("Bedford Park", "11654");

        System.out.println(mapOfNewYork);

        System.out.println(mapOfNewYork.get("Rego Park"));
        System.out.println(mapOfNewYork.get("Bedford Park"));
        System.out.println(mapOfNewYork.get("11374"));


    }
}
