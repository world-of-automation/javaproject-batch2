package refresh.datastructure;

import java.util.ArrayList;

public class ArrayListPractice {


    public static void main(String[] args) {

        ArrayList<String> arrayListOfStates = new ArrayList();
        arrayListOfStates.add("NY");
        arrayListOfStates.add("NJ");
        arrayListOfStates.add("VA");
        arrayListOfStates.add("DC");
        arrayListOfStates.add("TX");

        System.out.println(arrayListOfStates);

        System.out.println(arrayListOfStates.size());

        for (int i = 0; i < arrayListOfStates.size(); i++) {
            System.out.println(arrayListOfStates.get(i));
        }

        ArrayList<Integer> arrayListOfZip = new ArrayList();
        arrayListOfZip.add(11344);
        arrayListOfZip.add(53532);
        arrayListOfZip.add(56322);
        arrayListOfZip.add(66335);
        arrayListOfZip.add(88743);


        ArrayList<ArrayList> listOfUSA = new ArrayList();
        listOfUSA.add(arrayListOfStates);
        listOfUSA.add(arrayListOfZip);
        System.out.println(listOfUSA);


    }

}
