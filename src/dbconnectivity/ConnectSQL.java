package dbconnectivity;

import java.sql.*;

public class ConnectSQL {

    private static Connection connection;
    private static Statement statement;
    private static ResultSet resultSet;

    public static ResultSet connectToDatabase(String url, String userName, String password, String query) throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        connection = DriverManager.getConnection(url, userName, password);
        statement = connection.createStatement();
        resultSet = statement.executeQuery(query);
        return resultSet;
    }

    public static void clearDatabase() throws SQLException {
        resultSet.close();
        statement.close();
        connection.close();
    }


}
