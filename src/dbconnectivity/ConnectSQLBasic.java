package dbconnectivity;

import java.sql.*;

public class ConnectSQLBasic {

    public static void main(String[] args) throws ClassNotFoundException, SQLException {

        String userName = "root";
        String password = "root1234";
        String databaseName = "batch2";
        //jdbc:mysql - driver name
        //localhost - host name
        // 3306 - port number
        String url = "jdbc:mysql://localhost:3306/" + databaseName + "?serverTimezone=UTC";
        String query = "select * from location_information;";

        Class.forName("com.mysql.cj.jdbc.Driver");

        Connection connection = DriverManager.getConnection(url, userName, password);
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);

        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            int salary = resultSet.getInt("salary");
            System.out.println(id + " " + name + " " + salary);
        }

        resultSet.close();
        statement.close();
        connection.close();


    }
}
