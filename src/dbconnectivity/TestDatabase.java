package dbconnectivity;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TestDatabase {

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        String userName = "root";
        String password = "root1234";
        String databaseName = "batch2";
        String url = "jdbc:mysql://localhost:3306/" + databaseName + "?serverTimezone=UTC";
        String query = "select * from location_information;";

        ResultSet resultSet = ConnectSQL.connectToDatabase(url, userName, password, query);

        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            int salary = resultSet.getInt("salary");
            System.out.println(id + " " + name + " " + salary);
        }

        ConnectSQL.clearDatabase();
    }
}
