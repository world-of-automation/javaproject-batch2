package inputoutput;

import java.util.Scanner;

public class ScannerTest {

    public static void main(String[] args) {
        //myDetails();
        //calculator();
        scannerWithIfElse();
    }

    public static void myDetails() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("please enter your name");
        String name = scanner.next();
        System.out.println("please enter your age");
        int age = scanner.nextInt();
        System.out.println("value from scanner is " + name + age);
    }

    public static void calculator() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("please insert first number");
        int number1 = scanner.nextInt();

        System.out.println("please insert second number");
        int number2 = scanner.nextInt();

        int total = number1 + number2;
        System.out.println("total is : " + total);
    }

    public static void scannerWithIfElse() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("insert first value");
        int firstInt = scanner.nextInt();

        System.out.println("insert second value");
        int secondInt = scanner.nextInt();

        if (firstInt > secondInt) {
            System.out.println("first value is bigger number");
        } else if (firstInt == secondInt) {
            System.out.println("first value is equal to second");
        } else {
            System.out.println("second value is bigger number");
        }
    }

}
