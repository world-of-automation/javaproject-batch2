package inputoutput;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesFileReader {

    public static void main(String[] args) throws IOException {

        Properties properties = readPropertyFile("src/inputoutput/resources/testData.properties");

        String user = properties.getProperty("username");
        System.out.println(user);

        String pass = properties.getProperty("password");
        System.out.println(pass);

        //int result = calculator(4, 6);

    }

    public static Properties readPropertyFile(String filePath) throws IOException {
        Properties properties = new Properties();
        InputStream inputStream = new FileInputStream(filePath);
        properties.load(inputStream);
        return properties;
    }

    // for example
    public static int calculator(int a, int b) {
        int c = a + b;
        return c;
    }


}
