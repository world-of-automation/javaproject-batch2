package inputoutput;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class TextFileUtils {

    public static void main(String[] args) throws IOException {

        String data = readTextFile("src/inputoutput/resources/test.txt");
        System.out.println(data);

        String data2 = readTextFile("src/inputoutput/resources/test2.txt");
        System.out.println(data2);
    }


    // create a static method which will take a string param of file path
    // and return the texts of the file given


    public static String readTextFile(String filePath) throws IOException {
        String container;
        String finalText = "";

        FileReader fileReader = new FileReader(filePath);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        //BufferedReader bufferedReader2 = new BufferedReader(new FileReader(filePath));


        while ((container = bufferedReader.readLine()) != null) {
            finalText = finalText + "\n" + container;
        }

        return finalText;

    }

}
