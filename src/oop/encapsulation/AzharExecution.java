package oop.encapsulation;

public class AzharExecution {

    public static void main(String[] args) {
        Student student = new Student();
        student.setName("Azhar");
        String name = student.getName();
        System.out.println(name);
    }
}
