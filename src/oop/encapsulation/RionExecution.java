package oop.encapsulation;

public class RionExecution {

    public static void main(String[] args) {
        Student student = new Student();
        student.setName("Rion");
        String name = student.getName();
        System.out.println(name);
    }
}
