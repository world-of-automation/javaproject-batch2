package oop.plymorphism;

public class Calculator {
    // same name but diff params --> polymorphism (having many forms)
    public static int math(int a, int b) {
        return a + b;
    }

    public static int math(int a, int b, int c) {
        return a + b + c;
    }


    public static void main(String[] args) {

        int value = math(10, 10);
        System.out.println(value);

        int value2 = math(42, 10, 23);
        System.out.println(value2);
    }


}
