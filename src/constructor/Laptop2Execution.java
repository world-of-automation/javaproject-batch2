package constructor;

public class Laptop2Execution {

    public static void main(String[] args) {

        // non-static method
        // object creation -- line 10
        // className referrenceVariable_known_as_object = new constructor_Name();
        Laptop2 refVar = new Laptop2();
        refVar.printMyLaptopName();
        Laptop laptop = new Laptop();
        System.out.println(laptop.company);

        //static method
        Laptop2.printMyLaptopYear();
        System.out.println(Laptop.model);

    }
}
