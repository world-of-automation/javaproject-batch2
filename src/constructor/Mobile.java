package constructor;

public class Mobile {

    //constructor
    public Mobile() {

    }

    public static void main(String[] args) {
        //static
        goDrinkCoffee();
        String coffeeFromAzharVai = getMeCoffee();
        System.out.println(coffeeFromAzharVai);

        //non static
        //className referrenceVariable_known_as_object = new constructor_Name();
        Mobile mobile = new Mobile();
        mobile.printMyPhonesBrand();

    }

    //******* static method
    // void
    public static void goDrinkCoffee() {
        System.out.println("i went to dunkin and drank coffee");
    }

    // return type
    public static String getMeCoffee() {
        String drink = "coffee";
        return drink;
    }


    //******* non static method
    // void
    public void printMyPhonesBrand() {
        System.out.println("my phone is Samsung");
    }

    // return type
    public String getMyPhoneScreen() {
        String character = "touchscreen";
        return character;
    }

}
