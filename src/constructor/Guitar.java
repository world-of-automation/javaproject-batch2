package constructor;

public class Guitar {

    String brandOfGuitar;
    int numOfStringsInGuitar;
    String typeOfGuitar;


    public Guitar() {

    }

    public Guitar(String brandOfGuitar, int numOfStringsInGuitar, String typeOfGuitar) {
        this.brandOfGuitar = brandOfGuitar;
        this.numOfStringsInGuitar = numOfStringsInGuitar;
        this.typeOfGuitar = typeOfGuitar;
    }

    public Guitar(int numOfStringsInGuitar, String typeOfGuitar) {
        this.numOfStringsInGuitar = numOfStringsInGuitar;
        this.typeOfGuitar = typeOfGuitar;
    }

    public Guitar(String brandOfGuitar, String typeOfGuitar) {
        this.brandOfGuitar = brandOfGuitar;
        this.typeOfGuitar = typeOfGuitar;
    }

    public Guitar(String typeOfGuitar) {
        this.typeOfGuitar = typeOfGuitar;
    }


    public void pritMyGuitar() {
        System.out.println(brandOfGuitar);
        System.out.println(numOfStringsInGuitar);
        System.out.println(typeOfGuitar);
    }


}
