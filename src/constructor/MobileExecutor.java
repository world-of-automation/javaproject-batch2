package constructor;

public class MobileExecutor {

    public static void main(String[] args) {

        Mobile.goDrinkCoffee();

        String placeCoffeeinCup = Mobile.getMeCoffee();
        System.out.println(placeCoffeeinCup);

    }

}
