package constructor;

public class Laptop {

    // class level variables  --> global variables --> can have access specifiers

    static String model = "macbook pro";
    String company = "apple";

    // static  --> can go anywhere -- static method/non-static method
    // non static --> only goes to non-static

    public static void printMyModel() {
        System.out.println(model);
    }

    public static void main(String[] args) {
        System.out.println(model);
        Laptop laptop = new Laptop();
        System.out.println(laptop.company);
        printMyModel();
        Laptop refVar = new Laptop();
        refVar.printYear();
    }

    public void printCompany() {
        System.out.println(company);
    }

    public void printYear() {
        // method level variable
        int year = 2020;
        System.out.println(year);
    }

}
