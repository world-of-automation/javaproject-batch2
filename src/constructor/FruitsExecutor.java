package constructor;

public class FruitsExecutor {

    public static void main(String[] args) {
        // static void
        Fruits.printMango();

        // staic return type
        int mangoes = Fruits.getTheNumberofTheMangoes();
        System.out.println(mangoes);

        //non-static void
        Fruits fruits = new Fruits();
        fruits.printPineapple();

        //non-static return type
        String containerForFruits = fruits.getMyFavFruits();
        System.out.println(containerForFruits);

    }
}
