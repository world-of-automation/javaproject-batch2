package constructor;

public class Fruits {

    public Fruits() {

    }

    public static void main(String[] args) {
        // static void
        printMango();

        // static return type
        int mangoes = getTheNumberofTheMangoes();
        System.out.println(mangoes);

        //non-static void
        Fruits fruits = new Fruits();
        fruits.printPineapple();

        //non-static return type
        String containerForFruits = fruits.getMyFavFruits();
        System.out.println(containerForFruits);
    }

    // static

    // void
    public static void printMango() {
        System.out.println("mango");
    }

    // return type
    public static int getTheNumberofTheMangoes() {
        int mango = 10;
        return mango;
    }

    // non static

    // void
    public void printPineapple() {
        System.out.println("pineapple");
    }

    // return type
    public String getMyFavFruits() {
        String fruits = "Mango";
        return fruits;
    }

}
