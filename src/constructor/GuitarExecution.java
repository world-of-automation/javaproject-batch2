package constructor;

public class GuitarExecution {

    public static void main(String[] args) {

        Guitar guitar23 = new Guitar();
        //guitar23.pritMyGuitar();


        Guitar guitar = new Guitar("Jackson", 6, "Electric");
        //guitar.pritMyGuitar();


        Guitar guitar5 = new Guitar("Gibson", 12, "Electric");
        //guitar5.pritMyGuitar();


        Guitar guitar2 = new Guitar(12, "Acoustic");
        guitar2.pritMyGuitar();

        System.out.println("********");
        Guitar guitar4 = new Guitar("Charvel", "Acoustic");
        guitar4.pritMyGuitar();

        Guitar guitar3 = new Guitar("Ukulele");
        //guitar3.pritMyGuitar();
    }
}
