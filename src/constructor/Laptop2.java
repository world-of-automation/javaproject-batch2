package constructor;

public class Laptop2 {

    // not specify void/return
    // same name as class name + public
    // default constructor

    public Laptop2() {

    }

    // static method can be called using the class name
    // static doesn't needs constructor -- belongs to the class directly
    // same class -- no needs class name
    // different class -- needs the class name
    public static void printMyLaptopYear() {
        System.out.println(2019);
    }

    protected void printMyLaptopName() {
        System.out.println("MacBook Pro");
    }


}
