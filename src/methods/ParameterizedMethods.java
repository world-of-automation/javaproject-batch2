package methods;

public class ParameterizedMethods {

    public static int calculator2(int x, int z) {
        int total = x * z;
        return total;
    }

    public void printMyCar() {
        System.out.println("honda");
    }

    public void printMyName(String name) {
        System.out.println(name);
    }

    public void printMyLaptop(String model, String make) {
        System.out.println(make);
        System.out.println(model);
    }

    public int calculator(int a, int b) {
        int c = a + b;
        return c;
    }

}
