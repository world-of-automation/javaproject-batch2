package methods;

public class ReturnTypeMethods {

    public static void main(String[] args) {
        String name = getMyName();
        int id = getMyIDNumber();
        System.out.println(name + id);
        System.out.println(getMyName());
    }

    // return type method

    public static String getMyName() {
        return "Rion";
    }

    public static int getMyIDNumber() {
        int a = 10;
        int b = 20;
        int c = a + b;
        System.out.println(c);
        return c;
    }

}
