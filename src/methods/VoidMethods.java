package methods;

public class VoidMethods {

    public static void main(String[] args) {
        printMyName();
        printMyAge();
    }

    // void methods -- performs something for you -- doesn't come back/return with something

    public static void printMyName() {
        System.out.println("Rion");
    }

    public static void printMyAge() {
        System.out.println("26");
    }

}
