package methods;

public class ParameterizedMethodsExecution {

    public static void main(String[] args) {
        ParameterizedMethods parameterizedMethods = new ParameterizedMethods();
        parameterizedMethods.printMyCar();

        parameterizedMethods.printMyName("Rion");
        parameterizedMethods.printMyLaptop("MacBook", "Apple");

        int total = parameterizedMethods.calculator(10, 20);
        System.out.println(total);

        int totalFromMultiplication = ParameterizedMethods.calculator2(10, 2);
        System.out.println(totalFromMultiplication);
    }
}
