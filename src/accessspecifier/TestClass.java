package accessspecifier;

public class TestClass {

    public static void main(String[] args) {

    }


    static void method1() {

    }

    // public , private, protected, default  --> can be for methods & or for variables
    // public -- can be accessed anywhere in the project
    // private -- can be accessed in the same class only
    // protected -- can be access in the same package
    // default -- can be accessed in the same package and in the the subclass


}
